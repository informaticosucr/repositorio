﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa.Accesos
{
    public class UsuariosOperaciones
    {

        public static bool Guardar(Capa.Models.Usuarios pEntidad)
        {
            using (var cn = new MySqlConnection(DataConection.LeerCC))
            {
                // Contamos cuantos amigos existen segun el codamigo o nombre
                using (var cmd = new MySqlCommand(@"select ifnull(count(Nombre_Usuario),0) from Usuario where Nombre_Usuario=@nomUsuario or Nombre_Completo=@nomPersona;", cn))
                {
                    //Asignar valores a los parametros
                    cmd.Parameters.AddWithValue("nomUsuario", pEntidad.Nombre_Usuario);
                    cmd.Parameters.AddWithValue("nomPersona", pEntidad.Nombre_Completo);
                    cmd.Parameters.AddWithValue("contraseña", pEntidad.Contraseña);
                    cmd.Parameters.AddWithValue("Correo", pEntidad.Correo);
                    cmd.Parameters.AddWithValue("TipoUsua", pEntidad.Tipo_Usuario);
                    cmd.Parameters.AddWithValue("Fecha", pEntidad.Fecha_Creacion);
                  

                    cn.Open();
                    // Ejecutamos el comando y verificamos si el resultado es mayor a cero actualizar, caso contrario insertar
                    if (Convert.ToInt32(cmd.ExecuteScalar()) > 0)
                    {
                        // Si es mayor a cero, quiere decir que existe al menos un registro con los datos ingresados
                        // Entonces antes de actualizar, hacer las siguientes comprobaciones
                        if (pEntidad.Nombre_Usuario == "nomUsuario")
                            throw new Exception("El Usuario(a) ya esta registrado en el sistema, verifique los datos por favor!");
                    }
                    else
                        cmd.CommandText = @"insert into Usuario (Nombre_Completo, Nombre_Usuario, Contraseña, Correo , Tipo_Usuario,Fecha_Creacion) values (@nomPersona, @nomUsuario, @contraseña, @Correo, @TipoUsu, @Fecha);";

             return Convert.ToBoolean(cmd.ExecuteNonQuery());
                   
               }
            }
        }

        //public static List<Capa.Models.Usuarios> Leer(string dato)
        //{
        //    // Crea un obj. lista de tipo Amigo
        //    var lista = new List<Models.Usuarios>();
        //    // Crear el objeto de conexion
        //    using (var cn = new MySqlConnection(Capa.Accesos.DataConection.conexion.LeerCC))
        //    {
        //        // crear el comando
        //        using (var cmd = new MySqlCommand("select codamigo, nomdistrito, nombre, dni, fecnac, sexo, direccion, telefono from distritos inner join amigos on distritos.coddistrito = amigos.coddistrito where nombre like Concat(@nom, '%');", cn))
        //        {
        //            //Asignar valores a los parametros
        //            cmd.Parameters.AddWithValue("nom", dato);

        //            // Abrir el objeto de conexion
        //            cn.Open();
        //            using (var dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    // Crea un objeto del distrito
        //                    //var oDistrito = new Capa.Models.Usuarios();
        //                    var oUusarios = new Capa.Models.Usuarios();
        //                    // oAmigo.codamigo = Convert.ToInt32(dr[dr.GetOrdinal("codamigo")]);


        //                    oUusarios.Nombre_Usuario = Convert.ToString(dr[dr.GetOrdinal("nombreUsuario")]);
        //                    oUusarios.Nombre_Completo = Convert.ToString(dr[dr.GetOrdinal("dni")]);
        //                    oUusarios.Tipo_Usuario = Convert.ToDateTime(dr[dr.GetOrdinal("fecnac")]);
        //                    oUusarios.Fecha_Creacion = Convert.ToString(dr[dr.GetOrdinal("sexo")]);
        //                    oUusarios.Contraseña = Convert.ToString(dr[dr.GetOrdinal("direccion")]);
        //                    oUusarios.Correo = Convert.ToString(dr[dr.GetOrdinal("telefono")]);
        //                    // El objeto amigo es agregado a la lista
        //                    lista.Add(oUusarios);
        //                    // marcamos a los objetos creamos como nulos

        //                    oUusarios = null;
        //                }
        //            }

        //            // Retorna una lista de datos
        //            return lista;
        //        }
        //    }
        //}

    }
}
