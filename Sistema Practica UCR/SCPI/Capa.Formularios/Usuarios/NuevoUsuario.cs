﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capa.Formularios
{
    public partial class NuevoUsuario : Form
    {
        public NuevoUsuario()
        {
            InitializeComponent();
        }

        //objeto de la capa de models
        Capa.Models.Usuarios ManejaUsuarios = new Models.Usuarios();
       
     
        private void GuardarDatos(object sender, EventArgs e)
        {

      var ManejaUsuarios = new Capa.Models.Usuarios();
     
      ManejaUsuarios.Nombre_Completo= textNombreCompleto.Text.Trim();
            ManejaUsuarios.Correo = textCorreo.Text.Trim();
            ManejaUsuarios.Nombre_Usuario = textUsuario.Text.Trim();
            ManejaUsuarios.Tipo_Usuario = listBox1.Text.Trim();
            ManejaUsuarios.Contraseña = textContraseña.Text.Trim();
            ManejaUsuarios.Fecha_Creacion = DateTime.Now;
      try
      {
        Capa.Busines.Usuarios.Guardar(ManejaUsuarios);
        
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }
      finally { ManejaUsuarios = null; }
    }

       
        }
    }

