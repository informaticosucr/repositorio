﻿namespace Capa.Formularios
{
    partial class MenuExpedienteClinico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuExpedienteClinico));
            this.txtMedico = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.comboParentesco = new System.Windows.Forms.ComboBox();
            this.txtNombreParentesco = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtTelefParentesco = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.dateTimeFecha = new System.Windows.Forms.DateTimePicker();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTranstornosMenstruales = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.comboLactancia = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.comboEmbarazo = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.comboGenero = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtApartado = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTelefonoHabitacion = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtExtension = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTelefOficina = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtOcupacionPaciente = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCedulaPaciente = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ComboProvincia = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboCanton = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDireccionPaciente = new System.Windows.Forms.TextBox();
            this.txtRecomendadoPor = new System.Windows.Forms.TextBox();
            this.txtNomPaciente = new System.Windows.Forms.TextBox();
            this.txtNumeroExpediente = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnRegresarMenu = new System.Windows.Forms.Button();
            this.btnEliminarPaciente = new System.Windows.Forms.Button();
            this.btnBuscarPaciente = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtObservaciones = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.comboAlteracionSalud = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.comboOperaciones = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboAlergias = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtNomEnfermedad = new System.Windows.Forms.TextBox();
            this.comboEnfermedad = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.comboTratamiento = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnIngresarPaciente = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtMedico
            // 
            this.txtMedico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMedico.Location = new System.Drawing.Point(167, 416);
            this.txtMedico.Name = "txtMedico";
            this.txtMedico.Size = new System.Drawing.Size(279, 20);
            this.txtMedico.TabIndex = 88;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(29, 423);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(45, 13);
            this.label27.TabIndex = 87;
            this.label27.Text = "Médico:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.comboParentesco);
            this.groupBox3.Controls.Add(this.txtNombreParentesco);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.txtTelefParentesco);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox3.Location = new System.Drawing.Point(712, 52);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(521, 142);
            this.groupBox3.TabIndex = 86;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "En caso de emergencia avisar a ";
            // 
            // comboParentesco
            // 
            this.comboParentesco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboParentesco.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboParentesco.FormattingEnabled = true;
            this.comboParentesco.Items.AddRange(new object[] {
            "Padre",
            "Madre",
            "Suegro",
            "Suegra ",
            "Hijo ",
            "Hija ",
            "Yerno ",
            "Nuera ",
            "Abuelo",
            "Abuela ",
            "Hermano ",
            "Hermana ",
            "Cuñada ",
            "Cuñado ",
            "Nieto ",
            "",
            "Nieta ",
            "Tía ",
            "Tío ",
            "Sobrino",
            "Sobrina ",
            "Primo ",
            "Prima ",
            "Biznieto ",
            "Biznieta ",
            "Amigo ",
            "Amiga"});
            this.comboParentesco.Location = new System.Drawing.Point(92, 63);
            this.comboParentesco.Name = "comboParentesco";
            this.comboParentesco.Size = new System.Drawing.Size(279, 21);
            this.comboParentesco.TabIndex = 30;
            // 
            // txtNombreParentesco
            // 
            this.txtNombreParentesco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombreParentesco.Location = new System.Drawing.Point(92, 31);
            this.txtNombreParentesco.Name = "txtNombreParentesco";
            this.txtNombreParentesco.Size = new System.Drawing.Size(279, 20);
            this.txtNombreParentesco.TabIndex = 26;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(22, 36);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(47, 13);
            this.label31.TabIndex = 25;
            this.label31.Text = "Nombre:";
            // 
            // txtTelefParentesco
            // 
            this.txtTelefParentesco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelefParentesco.Location = new System.Drawing.Point(92, 96);
            this.txtTelefParentesco.Name = "txtTelefParentesco";
            this.txtTelefParentesco.Size = new System.Drawing.Size(279, 20);
            this.txtTelefParentesco.TabIndex = 24;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(22, 103);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 13);
            this.label26.TabIndex = 23;
            this.label26.Text = "Teléfono:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(22, 70);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(64, 13);
            this.label25.TabIndex = 21;
            this.label25.Text = "Parentesco:";
            // 
            // dateTimeFecha
            // 
            this.dateTimeFecha.Location = new System.Drawing.Point(515, 143);
            this.dateTimeFecha.Name = "dateTimeFecha";
            this.dateTimeFecha.Size = new System.Drawing.Size(123, 20);
            this.dateTimeFecha.TabIndex = 85;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(455, 150);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(40, 13);
            this.label24.TabIndex = 84;
            this.label24.Text = "Fecha:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTranstornosMenstruales);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.comboLactancia);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.comboEmbarazo);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox2.Location = new System.Drawing.Point(712, 241);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(521, 129);
            this.groupBox2.TabIndex = 83;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Damas";
            // 
            // txtTranstornosMenstruales
            // 
            this.txtTranstornosMenstruales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTranstornosMenstruales.Location = new System.Drawing.Point(240, 81);
            this.txtTranstornosMenstruales.Name = "txtTranstornosMenstruales";
            this.txtTranstornosMenstruales.Size = new System.Drawing.Size(143, 20);
            this.txtTranstornosMenstruales.TabIndex = 37;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(9, 88);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(225, 13);
            this.label30.TabIndex = 32;
            this.label30.Text = "¿Sufre transtornos durante el ciclo menstrual? ";
            // 
            // comboLactancia
            // 
            this.comboLactancia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboLactancia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboLactancia.FormattingEnabled = true;
            this.comboLactancia.Items.AddRange(new object[] {
            "Si",
            "No"});
            this.comboLactancia.Location = new System.Drawing.Point(181, 52);
            this.comboLactancia.Name = "comboLactancia";
            this.comboLactancia.Size = new System.Drawing.Size(42, 21);
            this.comboLactancia.TabIndex = 31;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(9, 60);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(159, 13);
            this.label29.TabIndex = 30;
            this.label29.Text = "¿Está en período de lactancia? ";
            // 
            // comboEmbarazo
            // 
            this.comboEmbarazo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboEmbarazo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboEmbarazo.FormattingEnabled = true;
            this.comboEmbarazo.Items.AddRange(new object[] {
            "Si",
            "No"});
            this.comboEmbarazo.Location = new System.Drawing.Point(181, 21);
            this.comboEmbarazo.Name = "comboEmbarazo";
            this.comboEmbarazo.Size = new System.Drawing.Size(42, 21);
            this.comboEmbarazo.TabIndex = 29;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(9, 29);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(157, 13);
            this.label28.TabIndex = 28;
            this.label28.Text = "¿Está en estado de embarazo? ";
            // 
            // txtCorreo
            // 
            this.txtCorreo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCorreo.Location = new System.Drawing.Point(167, 390);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(279, 20);
            this.txtCorreo.TabIndex = 82;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(29, 397);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 13);
            this.label16.TabIndex = 81;
            this.label16.Text = "Correo electrónico:";
            // 
            // comboGenero
            // 
            this.comboGenero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboGenero.FormattingEnabled = true;
            this.comboGenero.Items.AddRange(new object[] {
            "Masculino",
            "Femenino"});
            this.comboGenero.Location = new System.Drawing.Point(515, 173);
            this.comboGenero.Name = "comboGenero";
            this.comboGenero.Size = new System.Drawing.Size(122, 21);
            this.comboGenero.TabIndex = 80;
            this.comboGenero.SelectedIndexChanged += new System.EventHandler(this.comboGenero_SelectedIndexChanged_1);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(455, 181);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 13);
            this.label15.TabIndex = 79;
            this.label15.Text = "Género:";
            // 
            // txtApartado
            // 
            this.txtApartado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApartado.Location = new System.Drawing.Point(515, 364);
            this.txtApartado.Name = "txtApartado";
            this.txtApartado.Size = new System.Drawing.Size(122, 20);
            this.txtApartado.TabIndex = 78;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(455, 371);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 77;
            this.label13.Text = "Apdo.:";
            // 
            // txtTelefonoHabitacion
            // 
            this.txtTelefonoHabitacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelefonoHabitacion.Location = new System.Drawing.Point(167, 364);
            this.txtTelefonoHabitacion.Name = "txtTelefonoHabitacion";
            this.txtTelefonoHabitacion.Size = new System.Drawing.Size(279, 20);
            this.txtTelefonoHabitacion.TabIndex = 76;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(29, 371);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(119, 13);
            this.label12.TabIndex = 75;
            this.label12.Text = "Teléfono de habitación:";
            // 
            // txtExtension
            // 
            this.txtExtension.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtExtension.Location = new System.Drawing.Point(515, 336);
            this.txtExtension.Name = "txtExtension";
            this.txtExtension.Size = new System.Drawing.Size(122, 20);
            this.txtExtension.TabIndex = 74;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(455, 343);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 73;
            this.label11.Text = "Extensión:";
            // 
            // txtTelefOficina
            // 
            this.txtTelefOficina.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelefOficina.Location = new System.Drawing.Point(167, 336);
            this.txtTelefOficina.Name = "txtTelefOficina";
            this.txtTelefOficina.Size = new System.Drawing.Size(279, 20);
            this.txtTelefOficina.TabIndex = 72;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 343);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 13);
            this.label10.TabIndex = 71;
            this.label10.Text = "Teléfono de oficina:";
            // 
            // txtOcupacionPaciente
            // 
            this.txtOcupacionPaciente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOcupacionPaciente.Location = new System.Drawing.Point(167, 308);
            this.txtOcupacionPaciente.Name = "txtOcupacionPaciente";
            this.txtOcupacionPaciente.Size = new System.Drawing.Size(279, 20);
            this.txtOcupacionPaciente.TabIndex = 70;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 315);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 69;
            this.label9.Text = "Ocupación:";
            // 
            // txtCedulaPaciente
            // 
            this.txtCedulaPaciente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCedulaPaciente.Location = new System.Drawing.Point(167, 282);
            this.txtCedulaPaciente.Name = "txtCedulaPaciente";
            this.txtCedulaPaciente.Size = new System.Drawing.Size(279, 20);
            this.txtCedulaPaciente.TabIndex = 68;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 289);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 67;
            this.label8.Text = "N° de cédula:";
            // 
            // ComboProvincia
            // 
            this.ComboProvincia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboProvincia.FormattingEnabled = true;
            this.ComboProvincia.Location = new System.Drawing.Point(167, 255);
            this.ComboProvincia.Name = "ComboProvincia";
            this.ComboProvincia.Size = new System.Drawing.Size(279, 21);
            this.ComboProvincia.TabIndex = 66;
            this.ComboProvincia.SelectedIndexChanged += new System.EventHandler(this.ComboProvincia_SelectedIndexChanged_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 263);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 65;
            this.label7.Text = "Provincia:";
            // 
            // comboCanton
            // 
            this.comboCanton.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCanton.FormattingEnabled = true;
            this.comboCanton.Location = new System.Drawing.Point(515, 255);
            this.comboCanton.Name = "comboCanton";
            this.comboCanton.Size = new System.Drawing.Size(122, 21);
            this.comboCanton.TabIndex = 64;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(455, 263);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 63;
            this.label6.Text = "Cantón:";
            // 
            // txtDireccionPaciente
            // 
            this.txtDireccionPaciente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDireccionPaciente.Location = new System.Drawing.Point(167, 229);
            this.txtDireccionPaciente.Name = "txtDireccionPaciente";
            this.txtDireccionPaciente.Size = new System.Drawing.Size(279, 20);
            this.txtDireccionPaciente.TabIndex = 62;
            // 
            // txtRecomendadoPor
            // 
            this.txtRecomendadoPor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRecomendadoPor.Location = new System.Drawing.Point(167, 200);
            this.txtRecomendadoPor.Name = "txtRecomendadoPor";
            this.txtRecomendadoPor.Size = new System.Drawing.Size(279, 20);
            this.txtRecomendadoPor.TabIndex = 61;
            // 
            // txtNomPaciente
            // 
            this.txtNomPaciente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomPaciente.Location = new System.Drawing.Point(167, 174);
            this.txtNomPaciente.Name = "txtNomPaciente";
            this.txtNomPaciente.Size = new System.Drawing.Size(279, 20);
            this.txtNomPaciente.TabIndex = 60;
            // 
            // txtNumeroExpediente
            // 
            this.txtNumeroExpediente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumeroExpediente.Location = new System.Drawing.Point(167, 148);
            this.txtNumeroExpediente.Name = "txtNumeroExpediente";
            this.txtNumeroExpediente.Size = new System.Drawing.Size(108, 20);
            this.txtNumeroExpediente.TabIndex = 59;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 58;
            this.label5.Text = "Dirección:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "Recomendado por:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "Nombre del paciente:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "Número Expediente:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(511, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 24);
            this.label1.TabIndex = 54;
            this.label1.Text = "Menú Expediente Clínico";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.btnRegresarMenu);
            this.groupBox1.Controls.Add(this.btnEliminarPaciente);
            this.groupBox1.Controls.Add(this.btnBuscarPaciente);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(24, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(614, 90);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccione una opción";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(301, 69);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(90, 13);
            this.label34.TabIndex = 107;
            this.label34.Text = "Regresar al menú";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(185, 69);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(88, 13);
            this.label33.TabIndex = 106;
            this.label33.Text = "Eliminar Paciente";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(55, 69);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(85, 13);
            this.label32.TabIndex = 106;
            this.label32.Text = "Buscar Paciente";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(492, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(94, 68);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // btnRegresarMenu
            // 
            this.btnRegresarMenu.BackgroundImage = global::Capa.Formularios.Properties.Resources.icono_retroceso;
            this.btnRegresarMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRegresarMenu.Location = new System.Drawing.Point(317, 20);
            this.btnRegresarMenu.Name = "btnRegresarMenu";
            this.btnRegresarMenu.Size = new System.Drawing.Size(53, 50);
            this.btnRegresarMenu.TabIndex = 2;
            this.btnRegresarMenu.UseVisualStyleBackColor = true;
            // 
            // btnEliminarPaciente
            // 
            this.btnEliminarPaciente.BackgroundImage = global::Capa.Formularios.Properties.Resources.borrar;
            this.btnEliminarPaciente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminarPaciente.Location = new System.Drawing.Point(198, 20);
            this.btnEliminarPaciente.Name = "btnEliminarPaciente";
            this.btnEliminarPaciente.Size = new System.Drawing.Size(53, 50);
            this.btnEliminarPaciente.TabIndex = 1;
            this.btnEliminarPaciente.UseVisualStyleBackColor = true;
            this.btnEliminarPaciente.Click += new System.EventHandler(this.btnEliminarPaciente_Click_1);
            // 
            // btnBuscarPaciente
            // 
            this.btnBuscarPaciente.BackgroundImage = global::Capa.Formularios.Properties.Resources.busqueda_de_archivos_de_documentos_de_zoom_icono_4140_96;
            this.btnBuscarPaciente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuscarPaciente.Location = new System.Drawing.Point(71, 20);
            this.btnBuscarPaciente.Name = "btnBuscarPaciente";
            this.btnBuscarPaciente.Size = new System.Drawing.Size(53, 50);
            this.btnBuscarPaciente.TabIndex = 0;
            this.btnBuscarPaciente.UseVisualStyleBackColor = true;
            this.btnBuscarPaciente.Click += new System.EventHandler(this.btnBuscarPaciente_Click_1);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtObservaciones);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox4.Location = new System.Drawing.Point(713, 432);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(521, 188);
            this.groupBox4.TabIndex = 105;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Observaciones";
            // 
            // txtObservaciones
            // 
            this.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtObservaciones.Location = new System.Drawing.Point(12, 19);
            this.txtObservaciones.Multiline = true;
            this.txtObservaciones.Name = "txtObservaciones";
            this.txtObservaciones.Size = new System.Drawing.Size(482, 163);
            this.txtObservaciones.TabIndex = 38;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label23.Location = new System.Drawing.Point(660, 626);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(625, 13);
            this.label23.TabIndex = 102;
            this.label23.Text = "_________________________________________________________________________________" +
    "______________________";
            // 
            // comboAlteracionSalud
            // 
            this.comboAlteracionSalud.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboAlteracionSalud.FormattingEnabled = true;
            this.comboAlteracionSalud.Items.AddRange(new object[] {
            "Si",
            "No"});
            this.comboAlteracionSalud.Location = new System.Drawing.Point(400, 602);
            this.comboAlteracionSalud.Name = "comboAlteracionSalud";
            this.comboAlteracionSalud.Size = new System.Drawing.Size(42, 21);
            this.comboAlteracionSalud.TabIndex = 101;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(30, 610);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(364, 13);
            this.label22.TabIndex = 100;
            this.label22.Text = "¿Ha observado alguna alteración en la salud en general los últimos meses? ";
            // 
            // comboOperaciones
            // 
            this.comboOperaciones.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboOperaciones.FormattingEnabled = true;
            this.comboOperaciones.Items.AddRange(new object[] {
            "Si",
            "No"});
            this.comboOperaciones.Location = new System.Drawing.Point(400, 573);
            this.comboOperaciones.Name = "comboOperaciones";
            this.comboOperaciones.Size = new System.Drawing.Size(42, 21);
            this.comboOperaciones.TabIndex = 99;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(30, 581);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(289, 13);
            this.label21.TabIndex = 98;
            this.label21.Text = "¿Ha tenido operaciones médicas o ha estado internado(a)? ";
            // 
            // comboAlergias
            // 
            this.comboAlergias.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboAlergias.FormattingEnabled = true;
            this.comboAlergias.Items.AddRange(new object[] {
            "Aspirina ",
            "Penicilina ",
            "Sulfas ",
            "OtrosMedicamentos "});
            this.comboAlergias.Location = new System.Drawing.Point(202, 553);
            this.comboAlergias.Name = "comboAlergias";
            this.comboAlergias.Size = new System.Drawing.Size(130, 21);
            this.comboAlergias.TabIndex = 97;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(30, 556);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 13);
            this.label20.TabIndex = 96;
            this.label20.Text = "Alérgico(a)  a:";
            // 
            // txtNomEnfermedad
            // 
            this.txtNomEnfermedad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomEnfermedad.Location = new System.Drawing.Point(259, 520);
            this.txtNomEnfermedad.Name = "txtNomEnfermedad";
            this.txtNomEnfermedad.Size = new System.Drawing.Size(217, 20);
            this.txtNomEnfermedad.TabIndex = 95;
            // 
            // comboEnfermedad
            // 
            this.comboEnfermedad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboEnfermedad.FormattingEnabled = true;
            this.comboEnfermedad.Items.AddRange(new object[] {
            "Si",
            "No"});
            this.comboEnfermedad.Location = new System.Drawing.Point(202, 519);
            this.comboEnfermedad.Name = "comboEnfermedad";
            this.comboEnfermedad.Size = new System.Drawing.Size(42, 21);
            this.comboEnfermedad.TabIndex = 94;
            this.comboEnfermedad.SelectedIndexChanged += new System.EventHandler(this.comboEnfermedad_SelectedIndexChanged_1);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 527);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(168, 13);
            this.label19.TabIndex = 93;
            this.label19.Text = "¿Padece de alguna enfermedad? ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(30, 470);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(396, 13);
            this.label18.TabIndex = 92;
            this.label18.Text = "Indicar si el paciente presenta alguna de las siguientes condiciones:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label17.Location = new System.Drawing.Point(7, 626);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(631, 13);
            this.label17.TabIndex = 91;
            this.label17.Text = "_________________________________________________________________________________" +
    "_______________________";
            // 
            // comboTratamiento
            // 
            this.comboTratamiento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTratamiento.FormattingEnabled = true;
            this.comboTratamiento.Items.AddRange(new object[] {
            "Si",
            "No"});
            this.comboTratamiento.Location = new System.Drawing.Point(202, 491);
            this.comboTratamiento.Name = "comboTratamiento";
            this.comboTratamiento.Size = new System.Drawing.Size(42, 21);
            this.comboTratamiento.TabIndex = 90;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(30, 499);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(148, 13);
            this.label14.TabIndex = 89;
            this.label14.Text = "¿Recibe tratamiento médico? ";
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackgroundImage = global::Capa.Formularios.Properties.Resources.iconerror;
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelar.Location = new System.Drawing.Point(690, 642);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(47, 41);
            this.btnCancelar.TabIndex = 104;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click_1);
            // 
            // btnIngresarPaciente
            // 
            this.btnIngresarPaciente.BackgroundImage = global::Capa.Formularios.Properties.Resources.disquetes_excepto_icono_7120_64;
            this.btnIngresarPaciente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnIngresarPaciente.Location = new System.Drawing.Point(563, 642);
            this.btnIngresarPaciente.Name = "btnIngresarPaciente";
            this.btnIngresarPaciente.Size = new System.Drawing.Size(47, 41);
            this.btnIngresarPaciente.TabIndex = 103;
            this.btnIngresarPaciente.UseVisualStyleBackColor = true;
            this.btnIngresarPaciente.Click += new System.EventHandler(this.btnIngresarPaciente_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(547, 681);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(90, 13);
            this.label35.TabIndex = 106;
            this.label35.Text = "Guardar Paciente";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(688, 681);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(49, 13);
            this.label36.TabIndex = 107;
            this.label36.Text = "Cancelar";
            // 
            // MenuExpedienteClinico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1251, 703);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnIngresarPaciente);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.comboAlteracionSalud);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.comboOperaciones);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.comboAlergias);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtNomEnfermedad);
            this.Controls.Add(this.comboEnfermedad);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.comboTratamiento);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtMedico);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dateTimeFecha);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtCorreo);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.comboGenero);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtApartado);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtTelefonoHabitacion);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtExtension);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtTelefOficina);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtOcupacionPaciente);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtCedulaPaciente);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ComboProvincia);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.comboCanton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtDireccionPaciente);
            this.Controls.Add(this.txtRecomendadoPor);
            this.Controls.Add(this.txtNomPaciente);
            this.Controls.Add(this.txtNumeroExpediente);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MenuExpedienteClinico";
            this.Text = "Expediente";
            this.Load += new System.EventHandler(this.MenuExpedienteClinico_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMedico;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtTelefParentesco;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.DateTimePicker dateTimeFecha;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtTranstornosMenstruales;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox comboLactancia;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboEmbarazo;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboGenero;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtApartado;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTelefonoHabitacion;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtExtension;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtTelefOficina;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtOcupacionPaciente;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCedulaPaciente;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox ComboProvincia;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboCanton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDireccionPaciente;
        private System.Windows.Forms.TextBox txtRecomendadoPor;
        private System.Windows.Forms.TextBox txtNomPaciente;
        private System.Windows.Forms.TextBox txtNumeroExpediente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnRegresarMenu;
        private System.Windows.Forms.Button btnEliminarPaciente;
        private System.Windows.Forms.Button btnBuscarPaciente;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtObservaciones;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnIngresarPaciente;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox comboAlteracionSalud;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboOperaciones;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboAlergias;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtNomEnfermedad;
        private System.Windows.Forms.ComboBox comboEnfermedad;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboTratamiento;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNombreParentesco;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox comboParentesco;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;

    }
}