﻿namespace Capa.Formularios
{
    partial class Estado_Cuentas_Pagar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBuscarCuentaPagar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNombreProveedor = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnEliminarCuentaPagar = new System.Windows.Forms.Button();
            this.btnActualizarCuentaPagar = new System.Windows.Forms.Button();
            this.dataGridCuentaPagar = new System.Windows.Forms.DataGridView();
            this.btnSalir = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCuentaPagar)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNombreProveedor);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnBuscarCuentaPagar);
            this.groupBox1.Location = new System.Drawing.Point(39, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(766, 66);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Buscar Cuenta Específica";
            // 
            // btnBuscarCuentaPagar
            // 
            this.btnBuscarCuentaPagar.Location = new System.Drawing.Point(607, 24);
            this.btnBuscarCuentaPagar.Name = "btnBuscarCuentaPagar";
            this.btnBuscarCuentaPagar.Size = new System.Drawing.Size(75, 27);
            this.btnBuscarCuentaPagar.TabIndex = 0;
            this.btnBuscarCuentaPagar.Text = "Buscar";
            this.btnBuscarCuentaPagar.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ingrese el Nombre del Proveedor";
            // 
            // txtNombreProveedor
            // 
            this.txtNombreProveedor.Location = new System.Drawing.Point(297, 24);
            this.txtNombreProveedor.Name = "txtNombreProveedor";
            this.txtNombreProveedor.Size = new System.Drawing.Size(187, 22);
            this.txtNombreProveedor.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnActualizarCuentaPagar);
            this.groupBox2.Controls.Add(this.btnEliminarCuentaPagar);
            this.groupBox2.Location = new System.Drawing.Point(39, 102);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(766, 61);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Acciones de las Cuentas";
            // 
            // btnEliminarCuentaPagar
            // 
            this.btnEliminarCuentaPagar.Location = new System.Drawing.Point(186, 21);
            this.btnEliminarCuentaPagar.Name = "btnEliminarCuentaPagar";
            this.btnEliminarCuentaPagar.Size = new System.Drawing.Size(148, 23);
            this.btnEliminarCuentaPagar.TabIndex = 0;
            this.btnEliminarCuentaPagar.Text = "Eliminar";
            this.btnEliminarCuentaPagar.UseVisualStyleBackColor = true;
            // 
            // btnActualizarCuentaPagar
            // 
            this.btnActualizarCuentaPagar.Location = new System.Drawing.Point(488, 21);
            this.btnActualizarCuentaPagar.Name = "btnActualizarCuentaPagar";
            this.btnActualizarCuentaPagar.Size = new System.Drawing.Size(140, 23);
            this.btnActualizarCuentaPagar.TabIndex = 1;
            this.btnActualizarCuentaPagar.Text = "Actualizar";
            this.btnActualizarCuentaPagar.UseVisualStyleBackColor = true;
            // 
            // dataGridCuentaPagar
            // 
            this.dataGridCuentaPagar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridCuentaPagar.Location = new System.Drawing.Point(39, 201);
            this.dataGridCuentaPagar.Name = "dataGridCuentaPagar";
            this.dataGridCuentaPagar.RowTemplate.Height = 24;
            this.dataGridCuentaPagar.Size = new System.Drawing.Size(766, 198);
            this.dataGridCuentaPagar.TabIndex = 2;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(674, 458);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(105, 33);
            this.btnSalir.TabIndex = 3;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            // 
            // Estado_Cuentas_Pagar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 569);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.dataGridCuentaPagar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Estado_Cuentas_Pagar";
            this.Text = "Estado_Cuentas_Pagar";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridCuentaPagar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNombreProveedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBuscarCuentaPagar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnActualizarCuentaPagar;
        private System.Windows.Forms.Button btnEliminarCuentaPagar;
        private System.Windows.Forms.DataGridView dataGridCuentaPagar;
        private System.Windows.Forms.Button btnSalir;
    }
}