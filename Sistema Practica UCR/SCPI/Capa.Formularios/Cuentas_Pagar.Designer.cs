﻿namespace Capa.Formularios
{
    partial class Cuentas_Pagar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRegresaralmenu = new System.Windows.Forms.Button();
            this.btnVercuentasPagar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxMonto = new System.Windows.Forms.TextBox();
            this.textBoxNumeroFactura = new System.Windows.Forms.TextBox();
            this.dateTimeFechaActual = new System.Windows.Forms.DateTimePicker();
            this.dateTimeFVencimiento = new System.Windows.Forms.DateTimePicker();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(253, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cuentas por pagar";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.btnRegresaralmenu);
            this.groupBox1.Controls.Add(this.btnVercuentasPagar);
            this.groupBox1.Location = new System.Drawing.Point(28, 93);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(740, 100);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccione una opción";
            // 
            // btnRegresaralmenu
            // 
            this.btnRegresaralmenu.Location = new System.Drawing.Point(277, 45);
            this.btnRegresaralmenu.Name = "btnRegresaralmenu";
            this.btnRegresaralmenu.Size = new System.Drawing.Size(152, 33);
            this.btnRegresaralmenu.TabIndex = 1;
            this.btnRegresaralmenu.Text = "Regresar al Menú";
            this.btnRegresaralmenu.UseVisualStyleBackColor = true;
            // 
            // btnVercuentasPagar
            // 
            this.btnVercuentasPagar.Location = new System.Drawing.Point(61, 45);
            this.btnVercuentasPagar.Name = "btnVercuentasPagar";
            this.btnVercuentasPagar.Size = new System.Drawing.Size(160, 33);
            this.btnVercuentasPagar.TabIndex = 0;
            this.btnVercuentasPagar.Text = "Ver Cuentas";
            this.btnVercuentasPagar.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 225);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Proveedor:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(202, 218);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(200, 24);
            this.comboBox1.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 266);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "N. Factura:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 308);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Monto:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 348);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Fecha:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 384);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Fecha Vencimiento:";
            // 
            // textBoxMonto
            // 
            this.textBoxMonto.Location = new System.Drawing.Point(202, 303);
            this.textBoxMonto.Name = "textBoxMonto";
            this.textBoxMonto.Size = new System.Drawing.Size(200, 22);
            this.textBoxMonto.TabIndex = 8;
            // 
            // textBoxNumeroFactura
            // 
            this.textBoxNumeroFactura.Location = new System.Drawing.Point(202, 263);
            this.textBoxNumeroFactura.Name = "textBoxNumeroFactura";
            this.textBoxNumeroFactura.Size = new System.Drawing.Size(200, 22);
            this.textBoxNumeroFactura.TabIndex = 9;
            // 
            // dateTimeFechaActual
            // 
            this.dateTimeFechaActual.Location = new System.Drawing.Point(202, 348);
            this.dateTimeFechaActual.Name = "dateTimeFechaActual";
            this.dateTimeFechaActual.Size = new System.Drawing.Size(200, 22);
            this.dateTimeFechaActual.TabIndex = 10;
            // 
            // dateTimeFVencimiento
            // 
            this.dateTimeFVencimiento.Location = new System.Drawing.Point(202, 384);
            this.dateTimeFVencimiento.Name = "dateTimeFVencimiento";
            this.dateTimeFVencimiento.Size = new System.Drawing.Size(200, 22);
            this.dateTimeFVencimiento.TabIndex = 11;
            // 
            // btnIngresar
            // 
            this.btnIngresar.Location = new System.Drawing.Point(590, 416);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(118, 31);
            this.btnIngresar.TabIndex = 12;
            this.btnIngresar.Text = "Ingresar Cuenta";
            this.btnIngresar.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Capa.Formularios.Properties.Resources.CuentasPagar;
            this.pictureBox1.Location = new System.Drawing.Point(551, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(156, 73);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // Cuentas_Pagar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 471);
            this.Controls.Add(this.btnIngresar);
            this.Controls.Add(this.dateTimeFVencimiento);
            this.Controls.Add(this.dateTimeFechaActual);
            this.Controls.Add(this.textBoxNumeroFactura);
            this.Controls.Add(this.textBoxMonto);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "Cuentas_Pagar";
            this.Text = "Cuentas_Pagar";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnVercuentasPagar;
        private System.Windows.Forms.Button btnRegresaralmenu;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxMonto;
        private System.Windows.Forms.TextBox textBoxNumeroFactura;
        private System.Windows.Forms.DateTimePicker dateTimeFechaActual;
        private System.Windows.Forms.DateTimePicker dateTimeFVencimiento;
        private System.Windows.Forms.Button btnIngresar;
    }
}