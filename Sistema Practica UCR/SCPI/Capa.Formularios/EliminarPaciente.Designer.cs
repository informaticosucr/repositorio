﻿namespace Capa.Formularios
{
    partial class EliminarPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EliminarPaciente));
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTratamientoPacienteAeliminar = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTelefonoPacienteAeliminar = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNombrePacienteAeliminar = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCedulaPacienteAeliminar = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.btnRegresarMenu = new System.Windows.Forms.Button();
            this.btnEliminarPaciente = new System.Windows.Forms.Button();
            this.btnBuscarPaciente = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(208, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(331, 24);
            this.label1.TabIndex = 12;
            this.label1.Text = "Menú Eliminación de Expedientes";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.btnRegresarMenu);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.btnEliminarPaciente);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.btnBuscarPaciente);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtCedulaPacienteAeliminar);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtTratamientoPacienteAeliminar);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtTelefonoPacienteAeliminar);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtNombrePacienteAeliminar);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(37, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(666, 371);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 145);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(176, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "Datos del paciente a eliminar:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Tratamiento:";
            // 
            // txtTratamientoPacienteAeliminar
            // 
            this.txtTratamientoPacienteAeliminar.Location = new System.Drawing.Point(91, 240);
            this.txtTratamientoPacienteAeliminar.Name = "txtTratamientoPacienteAeliminar";
            this.txtTratamientoPacienteAeliminar.Size = new System.Drawing.Size(357, 20);
            this.txtTratamientoPacienteAeliminar.TabIndex = 39;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 213);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "Teléfono:";
            // 
            // txtTelefonoPacienteAeliminar
            // 
            this.txtTelefonoPacienteAeliminar.Location = new System.Drawing.Point(91, 206);
            this.txtTelefonoPacienteAeliminar.Name = "txtTelefonoPacienteAeliminar";
            this.txtTelefonoPacienteAeliminar.Size = new System.Drawing.Size(357, 20);
            this.txtTelefonoPacienteAeliminar.TabIndex = 37;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 180);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "Nombre:";
            // 
            // txtNombrePacienteAeliminar
            // 
            this.txtNombrePacienteAeliminar.Location = new System.Drawing.Point(91, 173);
            this.txtNombrePacienteAeliminar.Name = "txtNombrePacienteAeliminar";
            this.txtNombrePacienteAeliminar.Size = new System.Drawing.Size(357, 20);
            this.txtNombrePacienteAeliminar.TabIndex = 35;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "Ingrese el número de cédula:";
            // 
            // txtCedulaPacienteAeliminar
            // 
            this.txtCedulaPacienteAeliminar.Location = new System.Drawing.Point(156, 30);
            this.txtCedulaPacienteAeliminar.Name = "txtCedulaPacienteAeliminar";
            this.txtCedulaPacienteAeliminar.Size = new System.Drawing.Size(357, 20);
            this.txtCedulaPacienteAeliminar.TabIndex = 41;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(275, 105);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(85, 13);
            this.label32.TabIndex = 108;
            this.label32.Text = "Buscar Paciente";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(234, 328);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(88, 13);
            this.label33.TabIndex = 110;
            this.label33.Text = "Eliminar Paciente";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(352, 328);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 13);
            this.label34.TabIndex = 112;
            this.label34.Text = "Regresar ";
            // 
            // btnRegresarMenu
            // 
            this.btnRegresarMenu.BackgroundImage = global::Capa.Formularios.Properties.Resources.icono_retroceso;
            this.btnRegresarMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRegresarMenu.Location = new System.Drawing.Point(355, 283);
            this.btnRegresarMenu.Name = "btnRegresarMenu";
            this.btnRegresarMenu.Size = new System.Drawing.Size(47, 42);
            this.btnRegresarMenu.TabIndex = 111;
            this.btnRegresarMenu.UseVisualStyleBackColor = true;
            // 
            // btnEliminarPaciente
            // 
            this.btnEliminarPaciente.BackgroundImage = global::Capa.Formularios.Properties.Resources.borrar;
            this.btnEliminarPaciente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminarPaciente.Location = new System.Drawing.Point(249, 283);
            this.btnEliminarPaciente.Name = "btnEliminarPaciente";
            this.btnEliminarPaciente.Size = new System.Drawing.Size(47, 42);
            this.btnEliminarPaciente.TabIndex = 109;
            this.btnEliminarPaciente.UseVisualStyleBackColor = true;
            // 
            // btnBuscarPaciente
            // 
            this.btnBuscarPaciente.BackgroundImage = global::Capa.Formularios.Properties.Resources.busqueda_de_archivos_de_documentos_de_zoom_icono_4140_96;
            this.btnBuscarPaciente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuscarPaciente.Location = new System.Drawing.Point(294, 60);
            this.btnBuscarPaciente.Name = "btnBuscarPaciente";
            this.btnBuscarPaciente.Size = new System.Drawing.Size(47, 42);
            this.btnBuscarPaciente.TabIndex = 107;
            this.btnBuscarPaciente.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(551, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(94, 79);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // EliminarPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 446);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "EliminarPaciente";
            this.Text = "Expediente";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCedulaPacienteAeliminar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTratamientoPacienteAeliminar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTelefonoPacienteAeliminar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNombrePacienteAeliminar;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button btnBuscarPaciente;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button btnEliminarPaciente;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btnRegresarMenu;
    }
}