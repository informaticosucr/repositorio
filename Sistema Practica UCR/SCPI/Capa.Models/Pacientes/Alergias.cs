﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa.Models.Pacientes
{
    public enum  Alergias
    {
        Aspirina = 1,
        Penicilina = 2,
        Sulfas = 3,
        OtrosMedicamentos = 4
    }
}
