﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace Capa.Models.HistorialClinico
{
    public class Historial_Clinico
    {
        public string idHistorialClinico { get; set; }
        public string Tratamiento { get; set; }
        public DateTime Fecha { get; set; }
        public string Informe { get; set; }
        //public image Radiografia { get; set; }
        public DateTime Historico_Visita { get; set; }
        public string idPaciente { get; set; }
        public string Paciente_idPaciente { get; set; }
        public string Paciente_Cedula_Paciente { get; set; }

  //`idHistorialClinico` INT NOT NULL AUTO_INCREMENT COMMENT '',
  //`Tratamiento` VARCHAR(45) NOT NULL COMMENT '',
  //`Fecha` DATETIME NOT NULL COMMENT '',
  //`Informe` VARCHAR(45) NOT NULL COMMENT '',
  //`Radiografia` BLOB NULL COMMENT '',
  //`Historico_Visita` DATETIME NOT NULL COMMENT '',
  //`Id_Paciente` INT NOT NULL COMMENT '',
  //`Paciente_idPaciente` INT NOT NULL COMMENT '',
  //`Paciente_Cedula_Paciente` VARCHAR(10) NOT NULL COMMENT '',
    }
}
