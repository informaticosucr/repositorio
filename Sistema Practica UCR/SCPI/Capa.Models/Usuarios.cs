﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using System.ComponentModel.DataAnnotations;

namespace Capa.Models
{
    public class Usuarios
    {

        public string Id_Usuario { get; set; }
        public string Nombre_Completo { get; set; }
         public string Nombre_Usuario { get; set; }
         public string Contraseña { get; set; }
         public string Correo { get; set; }
         public string Tipo_Usuario { get; set; }
         public DateTime Fecha_Creacion { get; set; }
       

        
    }
}
