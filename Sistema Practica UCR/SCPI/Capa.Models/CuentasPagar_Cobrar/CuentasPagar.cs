﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa.Models.CuentasPagar_Cobrar
{
   public class CuentasPagar
    {

       public int idCuentasPagar { get; set; }
       public DateTime FechaPago_Agua { get; set; }
       public DateTime FechaPago_Luz { get; set; }
       public DateTime FechaPago_Internet { get; set; }
       public DateTime FechaPago_Asistente { get; set; }
       public DateTime FechaPago_Tributacion { get; set; }
       public DateTime FechaPago_Proveedores { get; set; }
       public float MontoPago_Asistente { get; set; }
       public float MontoPago_Tributaciones { get; set; }
       public float MontoPago_Proveedores { get; set; }
       public float Monto_ReciboLuz { get; set; }
       public float Monto_ReciboAgua { get; set; }
       public float Monto_ReciboInternet { get; set; }
      public int Proveedores_idProveedores { get; set; }
    


    }
}
