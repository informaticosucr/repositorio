﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa.Busines
{
    public class Pacientes
    {

        public void AgregarPaciente(Capa.Models.Pacientes.Paciente   item)
        {
            Capa.Accesos.PacientesOperaciones paciente = new Capa.Accesos.PacientesOperaciones();
            Capa.Models.Pacientes.Paciente nuevo = new Capa.Models.Pacientes.Paciente();

            nuevo.Nombre_Paciente = item.Nombre_Paciente;
            nuevo.Cedula_Paciente=item.Cedula_Paciente;
            nuevo.Telefono_Oficina=item.Telefono_Oficina;
            nuevo.Telefono_Habitacion=item.Telefono_Habitacion;
            nuevo.Ocupacion_Paciente=item.Ocupacion_Paciente;
            nuevo.Provincia_Paciente=item.Provincia_Paciente;
            nuevo.Canton_Paciente=item.Canton_Paciente;
            nuevo.Recibe_Tratamiento=item.Recibe_Tratamiento;
            nuevo.Direccion=item.Direccion;
            nuevo.Correo=item.Correo;
            nuevo.Recomendado=item.Recomendado;
            nuevo.Extension=item.Extension;
            nuevo.Apartado=item.Apartado;
            nuevo.Caso_Emergencia=item.Caso_Emergencia;
            nuevo.Genero=item.Genero;
            nuevo.Enfermedades=item.Enfermedades;
            nuevo.Parentezco=item.Parentezco;
            nuevo.TelefonoParentezco=item.TelefonoParentezco;
            nuevo.Medico=item.Medico;
            nuevo.Fecha=item.Fecha;
            nuevo.NumeroExpediente=item.NumeroExpediente;
            nuevo.OperacionesMedica=item.OperacionesMedica;
            nuevo.AlteracionSalud=item.AlteracionSalud;
            nuevo.Reacciones=item.Reacciones;
            nuevo.Embarazo=item.Embarazo;
            nuevo.Lactancia=item.Lactancia;
            nuevo.TrastornosMenstruales=item.TrastornosMenstruales;
            nuevo.Observaciones=item.Observaciones;

            //paciente.Agregar(nuevo);



        }
    }
}
