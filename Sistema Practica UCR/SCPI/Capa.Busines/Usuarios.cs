﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capa.Busines
{
    public class Usuarios
    {

        public static bool Guardar(Capa.Models.Usuarios pEntidad)
        {
            // Una regla del negocio, es que, el nombre del amigo(a) no podra ser un valor nulo o vacio
            // Sera obligatorio ingresar dicho dato
            if (string.IsNullOrEmpty(pEntidad.Nombre_Completo.Trim()))
                throw new Exception("El nombre del Usuario(a) no puede ser un valor nulo o vacio");

            // Otra regla del negocio es que el numero de dni debe ser de ocho caracteres
            if (pEntidad.Contraseña.Trim().Length < 7 | pEntidad.Contraseña.Trim().Length > 7)
                throw new Exception("La contraseña debera ser 5caracteres");

            return Capa.Accesos.UsuariosOperaciones.Guardar(pEntidad);
        }

    }
}
